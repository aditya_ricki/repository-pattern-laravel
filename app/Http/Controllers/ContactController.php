<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ContactService;
use Exception;

class ContactController extends Controller
{
	protected $request, $service, $result;

	function __construct(Request $request, ContactService $service)
	{
		$this->request = $request;
		$this->service = $service;
		$this->result = ['status' => 200, 'message' => 'Success'];
	}

	public function index()
	{
        try {
            $this->result['data'] = $this->service->getAll($this->request);
        } catch (Exception $e) {
            $this->result = [
                'status' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()->json($this->result, $this->result['status']);
	}

	public function indexPaginated()
	{
        try {
            $this->result['data'] = $this->service->getAllPaginated($this->request);
        } catch (Exception $e) {
            $this->result = [
                'status' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()->json($this->result, $this->result['status']);
	}

	public function show($id)
	{
        try {
            $this->result['data'] = $this->service->getById($id);
        } catch (Exception $e) {
            $this->result = [
                'status' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()->json($this->result, $this->result['status']);
	}

	public function store()
	{
        try {
            $this->result['data'] = $this->service->create($this->request);
        } catch (Exception $e) {
            $this->result = [
                'status' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()->json($this->result, $this->result['status']);
	}

	public function update($id)
	{
        try {
            $this->result['data'] = $this->service->update($this->request, $id);
        } catch (Exception $e) {
            $this->result = [
                'status' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()->json($this->result, $this->result['status']);
	}

	public function destroy($id)
	{
        try {
            $this->result['data'] = $this->service->delete($id);
        } catch (Exception $e) {
            $this->result = [
                'status' => 500,
                'message' => $e->getMessage()
            ];
        }

        return response()->json($this->result, $this->result['status']);
	}
}
