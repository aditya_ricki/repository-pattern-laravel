<?php

namespace App\Repositories\Contact;

use Illuminate\Http\Request;
use App\Repositories\Contact\ContactInterface;
use App\Models\Contact;

/**
 *
 */
class EloquentRepository implements ContactInterface
{
	protected $request, $model;

	function __construct(Request $request, Contact $model)
	{
		$this->model = $model;
	}

	public function getById($id)
	{
		return $this->model
			->whereId($id)
			->first();
	}

	public function getAll($request)
	{
		return $this->model
			->when($request->name, function ($query) use ($request) {
				$query->where('name', 'like', "%$request->name%");
			})
			->when($request->number, function ($query) use ($request) {
				$query->where('number', 'like', "%$request->number%");
			})
			->when($request->active, function ($query) use ($request) {
				$query->where('active', $request->active);
			})
			->latest()
			->get();
	}

	public function getAllPaginated($request)
	{
		return $this->model
			->when($request->name, function ($query) use ($request) {
				$query->where('name', 'like', "%$request->name%");
			})
			->when($request->number, function ($query) use ($request) {
				$query->where('number', 'like', "%$request->number%");
			})
			->when($request->active, function ($query) use ($request) {
				$query->where('active', $request->active);
			})
			->latest()
			->paginate($request->per_page ?? 10);
	}

	public function create($request)
	{
		return $this->model->create([
			'name' => $request->name,
			'number' => $request->number,
			'active' => 1,
		]);
	}

	public function update($request, Contact $model)
	{
		$model->update([
			'name' => $request->name,
			'number' => $request->number,
			'active' => 1,
		]);

		return $model;
	}

	public function delete(Contact $model)
	{
		$model->delete();

		return $model;
	}
}