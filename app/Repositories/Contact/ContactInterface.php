<?php

namespace App\Repositories\Contact;

use App\Models\Contact;

interface ContactInterface
{
	public function getById($id);
	public function getAll($request);
	public function getAllPaginated($request);
	public function create($request);
	public function update($request, Contact $model);
	public function delete(Contact $model);
}