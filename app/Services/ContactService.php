<?php

namespace App\Services;

use App\Repositories\Contact\EloquentRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

/**
 *
 */
class ContactService
{
	private $repository;

	function __construct(EloquentRepository $repository)
	{
		$this->repository = $repository;
	}

	public function getById($id)
	{
		try {
			return $this->repository->getById($id);
        } catch (Exception $e) {
            Log::info($e->getMessage());
            throw new InvalidArgumentException('Unable to get data by id');
        }
	}

	public function getAll($request)
	{
		try {
			return $this->repository->getAll($request);
        } catch (Exception $e) {
            Log::info($e->getMessage());
            throw new InvalidArgumentException('Unable to get all data');
        }
	}

	public function getAllPaginated($request)
	{
		try {
			return $this->repository->getAllPaginated($request);
        } catch (Exception $e) {
            Log::info($e->getMessage());
            throw new InvalidArgumentException('Unable to get all paginated data');
        }
	}

	public function create($request)
	{
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'number' => 'required'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

		try {
			$create = $this->repository->create($request);
        } catch (Exception $e) {
            DB::rollback();
            Log::info($e->getMessage());
            throw new InvalidArgumentException('Unable to create data');
        }

        DB::commit();
        return $create;
	}

	public function update($request, $id)
	{
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'number' => 'required'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

		try {
			$data = $this->repository->getById($id);

			if (!$data) {
				throw new InvalidArgumentException('Get one data return null');
			}

			$update = $this->repository->update($request, $data);
        } catch (Exception $e) {
            DB::rollback();
            Log::info($e->getMessage());
            throw new InvalidArgumentException('Unable to update data');
        }

        DB::commit();
        return $update;
	}

	public function delete($id)
	{
        DB::beginTransaction();

		try {
			$data = $this->repository->getById($id);

			if (!$data) {
				throw new InvalidArgumentException('Get one data return null');
			}

			$delete = $this->repository->delete($data);
        } catch (Exception $e) {
            DB::rollback();
            Log::info($e->getMessage());
            throw new InvalidArgumentException('Unable to delete data');
        }

        DB::commit();
        return $delete;
	}
}